# Kelompok F08 :
| Nama | NRP |
| ---------------------- | ---------- |
| Faiz Haq Noviandra | 5025211132 |
| Nayya Kamila Putri Y | 5025211183 |
| Cavel Ferrari | 5025211198 |

## Soal 1
 
### Point A
 
#### Deskripsi
 
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.
 
kaggle datasets download -d bryanb/fifa-player-stats-database
 
Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut. Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya.
 
#### Penyelesaian
 
Pertama kita buat terlebih dahulu file storage.c lalu untuk mendownload kita lakukan perintah berikut :
 
```c
const char* kaggleCommand = "kaggle datasets download -d bryanb/fifa-player-stats-database";
 
    system(kaggleCommand);
```
 
Kemudian untuk unzip hasil downloadan tersebut, kita lakukan perintah berikut :
 
```c
char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
```
 
Agar, file dapat didownload kita perlu mengkonfigurasi terlebih dahulu file kaggle.json yang didapat dari kaggle.com dan perlu kita lakukan perintah `pip install kaggle` terlebih dahulu pada terminal
 
#### Code
 
```c
void unzip(char *sourceDir){
  pid_t unzipID = fork();
 
  if (unzipID == 0){
    char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }
 
  waitpid(unzipID, NULL, 0);
  remove(sourceDir);
}
 
void download(){
    const char* kaggleCommand = "kaggle datasets download -d bryanb/fifa-player-stats-database";
 
    system(kaggleCommand);
}
```
 
#### Penjelasan
 
1. `char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL}` : Untuk mengunzip file hasil download lalu meremove file .zip yang telah diunzip
2. `system(kaggleCommand)` : Untuk menjalankan perintah `kaggle datasets download -d bryanb/fifa-player-stats-database` menggunakan fungsi system
 
### Point B
 
#### Deskripsi
 
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.
 
#### Penyelesaian
 
Soal tersebut dapat diselesaikan menggunakan perintah AWK, kita dapat menjalankan perintah sebagai berikut untuk menampilkan nama, club, usia, potential, nationality, serta photourl dari masing masing pemain yang telah difilter
 
```c
system("awk -F\",\" 'NR > 1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"Name: %s\\n\", $2; printf \"Club: %s\\n\", $9; printf \"Age: %s\\n\", $3; printf \"Potential: %s\\n\", $8; printf \"Nationality: %s\\n\", $5; printf \"Photo URL: %s\\n\", $4; printf \"---------------\\n\" }' FIFA23_official_data.csv");
```
 
#### Code
 
```c
system("awk -F\",\" 'NR > 1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"Name: %s\\n\", $2; printf \"Club: %s\\n\", $9; printf \"Age: %s\\n\", $3; printf \"Potential: %s\\n\", $8; printf \"Nationality: %s\\n\", $5; printf \"Photo URL: %s\\n\", $4; printf \"---------------\\n\" }' FIFA23_official_data.csv");
```
 
#### Penjelasan
 
1. `awk` : Untuk menjalankan program awk
2. `-F\",\"` : Untuk mengatur pemisah (field separator) dalam data menjadi koma (",")
3. `'NR > 1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\"` : Merupakan kondisi filter yang digunakan untuk memilih baris-baris data yang memenuhi kriteria usia di bawah 25 dan potential lebih dari 85 serta tidak berasal dari club manchester city
 
### Point C
 
#### Deskripsi
 
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.
 
#### Penyelesaian
 
Kita buat file baru dengan nama Dockerfile lalu kita gunakan base image sesuai sistem operasi yang kita gunakan, kemudian kita setup environmentnya dengan sesuai agar dapat menjalankan program storage.c, untuk setup pertama kita tentukan terlebih dahulu working directorynya menggunakan perintah berikut `WORKDIR /app` kemudian kita copy file kaggle.json serta storage.c ke dalam working directory kita pada docker menggunakan perintah berikut `COPY storage.c /app/` `COPY kaggle.json /root/.kaggle/kaggle.json` lalu kita lakukan perintah `RUN apt-get update && apt-get install -y gcc gawk python3 python3-pip unzip` untuk menginstall gcc, awk, python, dan unzip. Setelah itu kita jalankan perintah `RUN pip3 install kaggle` untuk menginstall kaggle pada docker dan kita compile program c kita dengan menjalankan perintah berikut `RUN gcc storage.c -o storage`, kemudian kita run program storage yang telah dicompile menggunakan perintah `CMD ["./storage"]`. Lalu setelah itu kita build dockerfile tersebut dengan perintah `sudo docker build -t storage-app .`, kemudian untuk menjalankan docker image yang telah kita build, kita ketikkan perintah berikut `sudo docker run storage-app`
 
#### Code
 
```Dockerfile
FROM kalilinux/kali-rolling
 
WORKDIR /app
 
COPY storage.c /app/
COPY kaggle.json /root/.kaggle/kaggle.json
 
RUN apt-get update && apt-get install -y gcc gawk python3 python3-pip unzip
 
RUN pip3 install kaggle
 
RUN gcc storage.c -o storage
 
CMD ["./storage"]
 
```
 
#### Penjelasan
 
1. `COPY storage.c /app/` : Untuk mengcopy storage.c ke working directory
2. `RUN apt-get update && apt-get install -y gcc gawk python3 python3-pip unzip` : Untuk menjalankan perintah `apt-get update` serta `apt-get install -y gcc gawk python3 python3-pip unzip`
3. `CMD ["./storage"]` : Untuk menjalankan perintah ./storage pada terminal
 
### Point D
 
#### Deskripsi
 
Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.
 
#### Penyelesaian
 
Kita perlu membuat akun dockerhub terlebih dahulu, lalu kita jalankan perintah `sudo docker login` lalu kita masukkan username dan password sesuai dengan akun yang telah kita buat, setelah itu kita lakukan perintah `sudo docker push dagdo/storage-app:latest` dimana dagdo03 merupakan username akun dockerhub saya, storage-app merupakan nama docker image yang telah saya build dan latest merupakan nama tag yang telah saya berikan ketika membuild docker image. Setelah itu file docker dapat dilihat pada link berikut `https://hub.docker.com/r/dagdo03/storage-app`
 
### Point E
 
#### Deskripsi
 
Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.
 
#### Penyelesaian
 
Pertama dibuat file baru dengan nama `docker-compose.yml`. Lalu kita setting agar docker dapat menjalankan instance sebanyak 5 sekaligus dengan susunan sebagai berikut
 
```yml
barcelona:
  image: dagdo03/storage-app
  build:
    context: ./Barcelona
    dockerfile: Dockerfile
  deploy:
    replicas: 5
  volumes:
    - .:/app/barcelona
```
 
dimana kita gunakan docker compose versi 3, lalu kita buat service dengan nama barcelona, napoli, dan storage-app dimana kita gunakan Dockerfile untuk membangun docker image pada servis yang kita buat lalu kita lakukan mounting volume dari host ke dalam kontainer untuk service tersebut. Lalu setelah itu kita buat direktori baru dengan nama Barcelona dan Napoli, lalu pada terminal kita lakukan perintah cd ke salah satu direktori tersebut. Setelah itu kita jalankan perintah `sudo docker-compose up` pada terminal untuk menjalankan docker-compose yang telah kita buat




### No 2
Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamny
Contoh: /jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt
Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.
Case invalid untuk bypass:
/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt
Case valid untuk bypass:
/jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt
Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Untuk mengujinya, maka lakukan hal-hal berikut:
# Poin A
Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.

Untuk menyelesaikan tugas tersebut, langkah pertama yang perlu kita lakukan adalah membuat sebuah aplikasi fuse yang memiliki kemampuan untuk membaca dan mengambil isi dari sebuah direktori. Dalam library fuse, untuk menciptakan fungsi tersebut, kita perlu mendefinisikan sebuah struktur sebagai berikut:
```
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
}
```
Lalu membuat Fungsi xmp_getattr: yang digunakan untuk mendapatkan atribut dari sebuah file atau direktori.
```
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    sprintf(fpath,"%s%s",dirpath,path);
    res = lstat(fpath, stbuf);
    if (res == -1) return -errno;
    return 0;
}
```
Fungsi xmp_read: ini digunakan untuk membaca isi dari sebuah file.
```
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
char fpath[1000];
if(strcmp(path,"/") == 0)
{
path=dirpath;
sprintf(fpath,"%s",path);
}
else sprintf(fpath, "%s%s",dirpath,path);
int res = 0;
int fd = 0 ;
(void) fi;
fd = open(fpath, O_RDONLY);
if (fd == -1) return -errno;
res = pread(fd, buf, size, offset);
if (res == -1) res = -errno;
close(fd);
return res;
}
```
Fungsi `xmp_mkdir` digunakan untuk menciptakan direktori baru. Fungsi ini menggabungkan jalur (path) dari direktori utama dengan jalur direktori baru yang diminta, dan kemudian memanggil fungsi mkdir untuk membuat direktori tersebut.
```
static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[10000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);


    char *desc = malloc(9000);
    if (strstr(fpath, "restricted") != NULL)
    {
        sprintf(desc, "Create directory %s", fpath);
        logged("FAILED", "MKDIR", desc);
        return -1;
    }

    res = mkdir(fpath, mode);

    sprintf(desc, "Create directory %s", fpath);
    logged("SUCCESS", "MKDIR", desc);

    if (res == -1) return -errno;
    return 0;

}
```

# Poin B dan C
Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/
Lalu, buat folder projectMagang di dalamnya. Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.
Berikut adalah penjelasan fungsi `xmp_rename` yang digunakan dalam aplikasi fuse untuk mengubah nama sebuah file atau direktori:
Fungsi `xmp_rename` digunakan untuk mengubah nama file atau direktori. Fungsi ini menggabungkan jalur (path) dari direktori utama dengan jalur file/direktori yang akan diubah namanya. Setelah itu, fungsi rename dipanggil untuk melakukan perubahan nama file/direktori tersebut. Dengan menggunakan fungsi rename, nama file atau direktori dapat diubah sesuai dengan input yang diberikan
Dalam konteks aplikasi fuse, fungsi `xmp_rename` akan dipanggil saat sistem file meminta untuk mengubah nama file atau direktori. Implementasi fungsi ini akan memanipulasi jalur dan memanggil fungsi rename untuk melakukan perubahan nama yang diperlukan
```static int xmp_rename(const char *from, const char *to)
{
    int res;
    char ffrom[1000];
    char fto[1000];

    if (strcmp(from, "/") == 0)
    {
        from = dirpath;
        sprintf(ffrom, "%s", from);
    }
    else sprintf(ffrom, "%s%s", dirpath, from);

    if (strcmp(to, "/") == 0)
    {
        to = dirpath;
        sprintf(fto, "%s", to);
    }
    else sprintf(fto, "%s%s", dirpath, to);

    int flag = 0;

    if (strstr(ffrom, "bypass") != NULL || strstr(ffrom, "sihir") != NULL)
    {
        flag =  1;
    }

    if (strstr(ffrom, "restricted") != NULL){
        flag = 0;
    }

    if (strstr(fto, "bypass") != NULL || strstr(fto, "sihir") != NULL)
    {
        flag =  1;
    }
    if (strstr(ffrom, "bypass") == NULL && strstr(ffrom, "sihir") == NULL && strstr(ffrom, "restricted") == NULL){
        flag = 1;
    }

    char *desc = malloc(9000);
    if (flag == 0){
        sprintf(desc, "Rename from %s to %s", ffrom, fto);
        logged("FAILED", "RENAME", desc);
        return -1;
    }

    sprintf(desc, "Rename from %s to %s", ffrom, fto);
    res = rename(ffrom, fto);

    if (res == -1) return -errno;
    logged("SUCCESS", "RENAME", desc);

    return 0;
}```

# Poin D
Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus
# Solusi
Dibutuhkan fungsi logged yang telah dibuat di poin A dan B yang bertujuan untuk membuat log. Berikut adalah fungsi dari logged
``` void logged(char *level, char *cmd, char *desc)
{
    FILE *fp;
   
    fp = fopen("/home/faiz/sisop/sisop-praktikum-modul-4-2023-ws-f08/soal2/logmucatatsini.txt", "a+");
    
    if (fp == NULL){
        printf("[Error] : [Gagal dalam membuka file]");
        exit(1);
    }
    char *user = getUserName();
    char *description = malloc(1000 * sizeof(char));
    sprintf(description, "%s-%s", user, desc);

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    fprintf(fp, "[%s]::%02d/%02d/%04d-%02d:%02d:%02d::[%s]::[%s]\n", level, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec, cmd, description);
    fclose(fp);
} ```
# Kendala Dalam Pengerjaan
Masih belum memahami lebih dalam mengenai FUSE

### No 3
Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.


## Poin A
Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c.


```c
// source path
static
const char * dirpath = "/home/faiz/inifolderetc/sisop";




// FUSE necessart operator


//  Inisisasi
static void * xmp_init(struct fuse_conn_info * conn) {
   (void) conn;
   char fpath[1000];


   const char * path = "/";
   if (strcmp(path, "/") == 0) {
      path = dirpath;
      sprintf(fpath, "%s", path);
   } else sprintf(fpath, "%s%s", dirpath, path);


   encode(fpath, 0);


   format(fpath);


   return NULL;
}




// get atribut
static int xmp_getattr(const char * path, struct stat * stbuf) {
   int res;
   char fpath[1000];


   sprintf(fpath, "%s%s", dirpath, path);


   res = lstat(fpath, stbuf);


   if (res == -1) return -errno;


   return 0;
}




// baca direktori
static int xmp_readdir(const char * path, void * buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info * fi) {


   // printf("call readdir(): %s\n", path);


   char fpath[1000];


   if (strcmp(path, "/") == 0) {
      path = dirpath;
      sprintf(fpath, "%s", path);
   } else sprintf(fpath, "%s%s", dirpath, path);


   int res = 0;


   DIR * dp;
   struct dirent * de;
   (void) offset;
   (void) fi;


   dp = opendir(fpath);


   if (dp == NULL) return -errno;


   while ((de = readdir(dp)) != NULL) {
      struct stat st;


      memset( & st, 0, sizeof(st));


      st.st_ino = de -> d_ino;
      st.st_mode = de -> d_type << 12;
      res = (filler(buf, de -> d_name, & st, 0));


      if (res != 0) break;
   }


   closedir(dp);


   return 0;
}




// baca file
static int xmp_read(const char * path, char * buf, size_t size, off_t offset, struct fuse_file_info * fi) {
   char fpath[1000];
   if (strcmp(path, "/") == 0) {
      path = dirpath;


      sprintf(fpath, "%s", path);
   } else sprintf(fpath, "%s%s", dirpath, path);


   int res = 0;
   int fd = 0;


   (void) fi;


   fd = open(fpath, O_RDONLY);


   if (fd == -1) return -errno;


   res = pread(fd, buf, size, offset);


   if (res == -1) res = -errno;


   close(fd);


   return res;
}




// struct operator
static struct fuse_operations xmp_oper = {
   .init = xmp_init,
   .getattr = xmp_getattr,
   .readdir = xmp_readdir,
   .read = xmp_read,


};




// main
int main(int argc, char * argv[]) {
   umask(0);
   
   // fuse main
   return fuse_main(argc, argv, & xmp_oper, NULL);


}
```


penjelasan:


menggunakan template seperti pada materi modul 4 dengan menambahkan operator:


* xmp_init : untuk inisisasi FUSE (menerapkan encoding pada poin B dan format penamaan pada poin c)


## poin B
Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).


```c
// fungsi encode file
void encodeFile(const char * filePath) {
   FILE * file = fopen(filePath, "rb");
   if (!file) {
      fprintf(stderr, "Gagal membuka file: %s\n", filePath);
      return;
   }


   // hitung ukuran file
   fseek(file, 0, SEEK_END);
   long fileSize = ftell(file);
   fseek(file, 0, SEEK_SET);


   // Alokasikan memory untuk isi file
   unsigned char * fileContent = malloc(fileSize);
   if (!fileContent) {
      fprintf(stderr, "Gagal mengalokasikan memori.\n");
      fclose(file);
      return;
   }


   // baca isi file
   if (fread(fileContent, 1, fileSize, file) != fileSize) {
      fprintf(stderr, "Gagal membaca file: %s\n", filePath);
      fclose(file);
      free(fileContent);
      return;
   }


   // hitung ukuran output base64
   long outputSize = 4 * ((fileSize + 2) / 3);


   // alokasikan memori untuk output base64
   char * base64Output = malloc(outputSize + 1);
   if (!base64Output) {
      fprintf(stderr, "Gagal Mengalokasikan memori.\n");
      fclose(file);
      free(fileContent);
      return;
   }


   // Encode file ke base64
   int i, j;
   for (i = 0, j = 0; i < fileSize; i += 3, j += 4) {
      unsigned char byte1 = fileContent[i];
      unsigned char byte2 = (i + 1 < fileSize) ? fileContent[i + 1] : 0;
      unsigned char byte3 = (i + 2 < fileSize) ? fileContent[i + 2] : 0;


      unsigned char index1 = byte1 >> 2;
      unsigned char index2 = ((byte1 & 0x03) << 4) | (byte2 >> 4);
      unsigned char index3 = ((byte2 & 0x0F) << 2) | (byte3 >> 6);
      unsigned char index4 = byte3 & 0x3F;


      base64Output[j] = base64Chars[index1];
      base64Output[j + 1] = base64Chars[index2];
      base64Output[j + 2] = (i + 1 < fileSize) ? base64Chars[index3] : '=';
      base64Output[j + 3] = (i + 2 < fileSize) ? base64Chars[index4] : '=';
   }
   base64Output[j] = '\0';


   // tutup file
   fclose(file);


   // write encoded base64 ke temporary file
   FILE * tempFile = fopen("temp.txt", "wb");
   if (!tempFile) {
      fprintf(stderr, "Gagal membuat temporary file.\n");
      free(fileContent);
      free(base64Output);
      return;
   }


   if (fwrite(base64Output, 1, strlen(base64Output), tempFile) != strlen(base64Output)) {
      fprintf(stderr, "Gagal melakukan write ke temporary file\n");
   }


   // tutup temporary file
   fclose(tempFile);


   // hapus file original
   if (remove(filePath) != 0) {
      fprintf(stderr, "Gagal menghapus original file: %s\n", filePath);
      free(fileContent);
      free(base64Output);
      return;
   }


   // ganti nama temporary file ke nama file original
   if (rename("temp.txt", filePath) != 0) {
      fprintf(stderr, "Gagal menngati nama temporary file.\n");
   } else {
      // printf("File berhasil di encoded\n");
   }


   // bersihkan resource
   free(fileContent);
   free(base64Output);
}


// fungsi encode direktori (berlaku secara rekursif)
void encode(const char * path, int flag) {
   DIR * dir;
   struct dirent * entry;
   struct stat fileStat;


   // buka direktory
   dir = opendir(path);
   if (!dir) {
      fprintf(stderr, "Direktori gagal untuk dibuka: %s\n", path);
      return;
   }


   // baca entri direktori
   while ((entry = readdir(dir)) != NULL) {


      // abaikan direktori sekrang dan direktori parent
      if (strcmp(entry -> d_name, ".") == 0 || strcmp(entry -> d_name, "..") == 0) {
         continue;
      }


      // buat full path untuk entri
      char fullpath[1024];
      snprintf(fullpath, sizeof(fullpath), "%s/%s", path, entry -> d_name);


      // dapatkan info entri
      if (stat(fullpath, & fileStat) < 0) {
         fprintf(stderr, "Failed to get file stat: %s\n", fullpath);
         continue;
      }


      if (S_ISDIR(fileStat.st_mode)) {
         // Entry adalah direktori
         // printf("Directory: %s\n", fullpath);


         // int flag = 0;


         char firstLetter = entry -> d_name[0];
         if (flag == 1 || firstLetter == 'l' || firstLetter == 'u' || firstLetter == 't' || firstLetter == 'h' || firstLetter == 'L' || firstLetter == 'U' || firstLetter == 'T' || firstLetter == 'H') {
            encode(fullpath, 1);
         } else {
            encode(fullpath, 0);
         }


         //encode direktorii secara rekursif


      } else if (S_ISREG(fileStat.st_mode)) {
         // entry adalah file biasa


         if (flag == 1) {
            // encode
            encodeFile(fullpath);
            // printf("File: %d %s\n", flag, fullpath);


         }
         //printf("File: %d %s\n", flag, fullpath);
      }
   }


   // tutup direktori
   closedir(dir);
}
```


Penjelasan encode():


1. Fungsi encode() akan dipanggil pada saat FUSE diinisiasi (xmp_init)
2. kemudian akan membuka direktori dan membaca entrynya.
3. jika entri adalah berupa direktori maka cek apakah direktori diawali dengan huruf ("L", "U", "T", atau "H") atau telah diberi flag, jika ya maka encode() direktori dengan flag = 1 dan path adalah path entri direktori, lalu kembali ke langkah 2. jika tidak maka  lakuka rekursi encode() direktori dengan flag = 0 dan path adalah path entri direktori. kembali ke langkah 2
4. jika entri berupa file biasa, cek apakah flag=1, jika ya encodeFile().
5. lakukan pada semua entri direktori


## Poin C
Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.


```c
void format(const char * path) {
   DIR * dir = opendir(path);
   if (!dir) {
      fprintf(stderr, "Gagal membuka direktori: %s\n", path);
      return;
   }


   struct dirent * entry;
   while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG) {
         // buat full path
         char oldFilePath[1024];
         snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", path, entry -> d_name);


         // buat nama file dengan lowercase
         char newFileName[1024];
         strcpy(newFileName, entry -> d_name);
         int len = strlen(newFileName);
         for (int i = 0; i < len; i++) {
            newFileName[i] = tolower(newFileName[i]);
         }


         // buat full path file baru
         char newFilePath[1030];
         snprintf(newFilePath, sizeof(newFilePath), "%s/%s", path, newFileName);


         len = strlen(newFileName);
         if (len <= 4) {
            char newName[256] = "";
            int div;
            size_t index = 0;


            for (int i = 0; i < len; i++) {
               div = 128; // 8th bit
               while (div) {
                  newName[index] = ((newFileName[i] & div) == div) ? '1' : '0';
                  index++;
                  if (div == 1 && i < len) {
                     newName[index] = ' ';
                     index++;
                  }
                  div /= 2;
               }
            }


            strcpy(newFileName, newName);
         }


         snprintf(newFilePath, sizeof(newFilePath), "%s/%s", path, newFileName);


         // Ganti nama file
         if (rename(oldFilePath, newFilePath) != 0) {
            fprintf(stderr, "Gagal mengganti nama file: %s\n", oldFilePath);
            continue;
         } else {
            // printf("sukses mengganti nama file: %s\n", newFilePath);
         }


      } else if (entry -> d_type == DT_DIR && strcmp(entry -> d_name, ".") != 0 && strcmp(entry -> d_name, "..") != 0) {
         // buat full path direktoru
         char oldDirPath[1024];
         snprintf(oldDirPath, sizeof(oldDirPath), "%s/%s", path, entry -> d_name);


         // buat nama direktori baru dengan uppercase semua
         char newDirName[1024];
         strcpy(newDirName, entry -> d_name);
         int len = strlen(newDirName);
         for (int i = 0; i < len; i++) {
            newDirName[i] = toupper(newDirName[i]);
         }


         // buat full path direktori baru
         char newDirPath[1500];
         snprintf(newDirPath, sizeof(newDirPath), "%s/%s", path, newDirName);


         len = strlen(newDirName);
         if (len <= 4) {


            char newName[256] = "";
            int div;
            size_t index = 0;


            for (int i = 0; i < len; i++) {


               div = 128; // 8th bit
               while (div != 0) {
                  newName[index] = ((newDirName[i] & div) == div) ? '1' : '0';
                  index++;
                  if (div == 1 && i < len - 1) {
                     newName[index] = ' ';
                     index++;
                  }
                  div /= 2;
               }


            }


            strcpy(newDirName, newName);
         }


         snprintf(newDirPath, sizeof(newDirPath), "%s/%s", path, newDirName);


         // Ganti nama direktori
         if (rename(oldDirPath, newDirPath) != 0) {
            fprintf(stderr, "Gagal mengganti nama direktori: %s\n", oldDirPath);
            continue;
         } else {
            // printf("sukses mengganti nama direktori: %s\n", newDirPath);
         }


         // pannggil function secara rekursif


         format(newDirPath);
      }


   }


   // Close the directory
   closedir(dir);
}
```


penjelasan forma():
1. Buka direktori
2. baca entri direktori
3. jika entri adalah file maka ubah nama file menjadi lower case semua. kemudian cek apakah jumlah huruf nama file <= 4, jika ya maka ubah ke format binary yang didapat dari ASCII code masing-masing karakter.
4. jika entri adalah direktori maka ubah nama direktori menjadi upper case semua. kemudian cek apakah jumlah huruf nama direktori <= 4. jika ya maka ubah ke format binary yang didapat dari ASCII code masing-masing karakter. lakukan rekursi format()dengan argumen path adalah path entri direktori


## Poin D
Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah)


## Poin E
Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
* Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut.
* Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun.


## Poin F
Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂



## Kendala 
1. Masih belum paham fuse dan docker











